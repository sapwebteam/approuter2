var controller;
sap.ui.define([
	"sap/ui/core/mvc/Controller"
],
	/**
	 * @param {typeof sap.ui.core.mvc.Controller} Controller
	 */
	function (Controller) {
		"use strict";
		return Controller.extend("uimodule2.controller.View1", {
            model : null,
			onInit: function () {
                controller = this;
                this.model = new sap.ui.model.odata.v4.ODataModel({
                        /* send requests directly. Use $auto for batch request wich will be send automatically on before rendering */
                    groupId : "$direct",
                        /* I'll just quote the API documentary:
                    Controls synchronization between different bindings which refer to the same data for the case data changes in one binding.
                    Must be set to 'None' which means bindings are not synchronized at all; all other values are not supported and lead to an error.
                    */
                    synchronizationMode : "None",
                    /*
                    Root URL of the service to request data from.
                    */
                        serviceUrl : "backend_cap_dest/",
                    /*
                    optional. Group ID that is used for update requests. If no update group ID is specified, mParameters.groupId is used.:
                    updateGroupId : "$direct"
                    */
                });
                this.getView().setModel(this.model);
            },
            onDelete: function(oEvent) {
                var that = this;
                var path = oEvent.getParameter('listItem').getBindingContext().getPath();
                var context = oEvent.getParameter('listItem').getBindingContext();

                that.getView().setBusy(true);
                // that.model.remove(path, {
                //     success: function(data) {
                //         that.getView().setBusy(false);
                //         sap.m.MessageToast.show("Book deleted");
                        
                //     },
                //     error: function(e) {
                //         that.getView().setBusy(false);
                //     }
                // });	
                context.delete("$auto").then(function () {
                    sap.m.MessageToast.show("Deleted Sales Order")
                    that.getView().setBusy(false);
                }, function (oError) {
                    that.getView().setBusy(false);
                });
            },
            addBook : function() {
                var obj = {};
                obj.ID = parseInt(Math.random() * 10000);
                obj.title = "Example book";
                obj.stock = parseInt(Math.random() * 100);
                var that = this;
                that.getView().setBusy(true);
                // this.model.create("/Books", obj, {
                //     success: function(data) {
                //         that.getView().setBusy(false);
                //         sap.m.MessageToast.show("Book created");
                //     },
                //     error: function(e) {
                //         that.getView().setBusy(false);
                //     }
                // });
                var oContext = this.getView().byId("bookTable").getBinding("items")
                    .create(obj);
 
                 // Note: This promise fails only if the transient entity is deleted
                oContext.created().then(function () {
                    that.getView().setBusy(false);
                    sap.m.MessageToast.show("Book created");
                }, function (oError) {
                    // handle rejection of entity creation; if oError.canceled === true then the transient entity has been deleted 
                    that.getView().setBusy(false);
                });
            }
		});
	});
